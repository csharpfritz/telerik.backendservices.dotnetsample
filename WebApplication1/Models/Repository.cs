using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Telerik.Everlive.Sdk.Core;
using Telerik.Everlive.Sdk.Core.Model.Base;
using Telerik.Everlive.Sdk.Core.Serialization;
using Telerik.Web.UI.PivotGrid.Core.Filtering;

namespace WebApplication1.Models
{
  public class Repository<T> where T : DataItem
  {

    private EverliveApp app;

    public Repository()
    {
      this.app = new EverliveApp(ConfigurationManager.AppSettings["apiKey"]);
      app.WorkWith().Authentication().Login(ConfigurationManager.AppSettings["apiUser"], ConfigurationManager.AppSettings["apiPass"]);
    }

    public IEnumerable<T> GetAll()
    {
      return Get(_ => true);
    }

    public Activity GetById(Guid id)
    {
      return app.WorkWith().Data<Activity>().GetById(id).ExecuteAsync().Result;
    }

    public IEnumerable<T> Get(Expression<Func<T, bool>> where)
    {
      return app.WorkWith().Data<T>().Get().ExecuteSync();
//      return app.WorkWith().Data<T>().Get().Where(where).ExecuteSync();
    }

    public void Save(T objToSave)
    {
      app.WorkWith().Data<T>().Update(objToSave).ExecuteSync();
    }

    public void Delete(Guid id)
    {
      app.WorkWith().Data<T>().Delete(id).ExecuteSync();
    }


  }

  [ServerType("Activities")]
  public class Activity : DataItem
  {

    private Guid _UserId;
    public Guid UserId
    {
      get
      {
        return _UserId;
      }
      set
      {
        _UserId = value;
        this.OnPropertyChanged("UserId");
      }
    }

    private Guid _Picture;
    public Guid Picture
    {
      get { return _Picture; }
      set
      {
        _Picture = value;
        this.OnPropertyChanged("Picture");
      }
    }

    private Guid[] _Likes;
    public Guid[] Likes
    {
      get
      {
        return _Likes;
      }
      set
      {
        _Likes = value;
        this.OnPropertyChanged("Likes");
      }
    }

    private string _Text;
    public string Text
    {
      get
      {
        return _Text;
      }
      set
      {
        _Text = value;
        this.OnPropertyChanged("Text");
      }
    }

  }


}