﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

      <asp:ListView runat="server" ID="activityList" ItemType="WebApplication1.Models.Activity" SelectMethod="activityList_GetData">
        <ItemTemplate>
          <p>
            <b>Text:</b> <%#: Item.Text %>
          </p>
        </ItemTemplate>
      </asp:ListView>

    </div>
    </form>
</body>
</html>
